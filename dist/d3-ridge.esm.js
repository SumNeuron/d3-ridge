import { scaleLinear, curveBasis, scaleSequential, interpolateViridis, easeSin, zoom, event, select, keys, extent, axisBottom, scaleBand, axisLeft, axisRight, line, mean } from 'd3';

if (typeof document !== "undefined") {
  var element = document.documentElement;
}

function kernelDensityEstimator(kernel, X) {
  return function (V) {
    return X.map(function (x) {
      return [x, mean(V, function (v) {
        return kernel(x - v);
      })];
    });
  };
}
function kernelEpanechnikov(k) {
  return function (v) {
    return Math.abs(v /= k) <= 1 ? 0.75 * (1 - v * v) / k : 0;
  };
}

var math = /*#__PURE__*/Object.freeze({
  kernelDensityEstimator: kernelDensityEstimator,
  kernelEpanechnikov: kernelEpanechnikov
});

/**
* Extracts x and y of translate from transform property
* @param {string} transform transform property of svg element
* @returns {number[]} x, y of translate(x, y)
*/
function getTranslation(transform) {
  // Create a dummy g for calculation purposes only. This will never
  // be appended to the DOM and will be discarded once this function
  // returns.
  var g = document.createElementNS('http://www.w3.org/2000/svg', 'g'); // Set the transform attribute to the provided string value.

  transform = transform == undefined ? 'translate(0,0)' : transform;
  g.setAttributeNS(null, 'transform', transform); // consolidate the SVGTransformList containing all transformations
  // to a single SVGTransform of type SVG_TRANSFORM_MATRIX and get
  // its SVGMatrix.

  var matrix = g.transform.baseVal.consolidate().matrix; // As per definition values e and f are the ones for the translation.

  return [matrix.e, matrix.f];
}
function resizeDebounce(f, wait) {
  var resize = debounce(function () {
    f();
  }, wait);
  window.addEventListener('resize', resize);
}
function debounce(func, wait, immediate) {
  var timeout;
  return function () {
    var context = this,
        args = arguments;

    var later = function later() {
      timeout = null;
      if (!immediate) func.apply(context, args);
    };

    var callNow = immediate && !timeout;
    clearTimeout(timeout);
    timeout = setTimeout(later, wait);
    if (callNow) func.apply(context, args);
  };
}

var utils = /*#__PURE__*/Object.freeze({
  getTranslation: getTranslation,
  resizeDebounce: resizeDebounce,
  debounce: debounce
});

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance"); }

function _iterableToArrayLimit(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }
function ridge(container) {
  var data,
      spaceX,
      spaceY,
      dataKeys,
      dataValues,
      dataExtent,
      scaleX = scaleLinear(),
      scaleY = scaleLinear(),
      valueExtractor = function valueExtractor(key, index) {
    return data[key];
  },
      curve = curveBasis,
      colorExtractor = function colorExtractor(key, index) {
    return scaleSequential().interpolator(interpolateViridis)(index / dataKeys.length);
  },
      opacity = 0.7,
      stroke = function stroke(k, i) {
    return 'black';
  },
      strokeWidth = 0.1,
      kernel = 7,
      kernelFineness = 40,
      axisDensityTicks = 4,
      axisXTicks = 5,
      mouseover = function mouseover(k, i) {},
      mouseleave = function mouseleave(k, i) {},
      namespace = 'ridge',
      axisX,
      axisY,
      axisYDensity,
      gAxisX,
      gAxisY,
      gAxisYDensity,
      areaContainer,
      categorySpaceY,
      adjustedSpaceY,
      scaleYCategories,
      gridlinesX = true,
      gridlinesY = true,
      includeDensityScale = true,
      applyZoom = true,
      transitionDuration = 500,
      easeFn = easeSin,
      zoom$$1 = zoom(),
      savedZoomState,
      zoomed = function zoomed() {
    savedZoomState = event.transform;
    areaContainer.attr("transform", savedZoomState);
    gAxisX // .transition(transitionDuration)
    // .ease(easeFn)
    .call(axisX.scale(savedZoomState.rescaleX(scaleX)));
    scaleYCategories.range([spaceY, spaceY - adjustedSpaceY].map(function (d) {
      return savedZoomState.applyY(d);
    }));
    gAxisY // .transition(transitionDuration)
    // .ease(easeFn)
    .call(axisY);
    scaleY.range([categorySpaceY, 0].map(function (d) {
      return savedZoomState.applyY(d);
    }));

    if (includeDensityScale) {
      gAxisYDensity // .transition(transitionDuration)
      // .ease(easeFn)
      .call(axisYDensity).attr('transform', "translate(".concat(spaceX, ",").concat(spaceY - savedZoomState.applyY(categorySpaceY), ")")).select('.domain').remove();

      var _max = savedZoomState.applyY(spaceY - adjustedSpaceY);

      gAxisYDensity.selectAll('.tick').each(function (d, i) {
        var dthis = select(this);

        var _getTranslation = getTranslation(dthis.attr('transform')),
            _getTranslation2 = _slicedToArray(_getTranslation, 2),
            x = _getTranslation2[0],
            y = _getTranslation2[1]; // can't figure out how to remove ticks above 0

      });
    }
  };

  ridge.data = function (_) {
    return arguments.length ? (data = _, ridge) : data;
  };

  ridge.spaceX = function (_) {
    return arguments.length ? (spaceX = _, ridge) : spaceX;
  };

  ridge.spaceY = function (_) {
    return arguments.length ? (spaceY = _, ridge) : spaceY;
  };

  ridge.dataKeys = function (_) {
    return arguments.length ? (dataKeys = _, ridge) : dataKeys;
  };

  ridge.dataValues = function (_) {
    return arguments.length ? (dataValues = _, ridge) : dataValues;
  };

  ridge.dataExtent = function (_) {
    return arguments.length ? (dataExtent = _, ridge) : dataExtent;
  };

  ridge.scaleX = function (_) {
    return arguments.length ? (scaleX = _, ridge) : scaleX;
  };

  ridge.scaleY = function (_) {
    return arguments.length ? (scaleY = _, ridge) : scaleY;
  };

  ridge.valueExtractor = function (_) {
    return arguments.length ? (valueExtractor = _, ridge) : valueExtractor;
  };

  ridge.curve = function (_) {
    return arguments.length ? (curve = _, ridge) : curve;
  };

  ridge.colorExtractor = function (_) {
    return arguments.length ? (colorExtractor = _, ridge) : colorExtractor;
  };

  ridge.opacity = function (_) {
    return arguments.length ? (opacity = _, ridge) : opacity;
  };

  ridge.stroke = function (_) {
    return arguments.length ? (stroke = _, ridge) : stroke;
  };

  ridge.strokeWidth = function (_) {
    return arguments.length ? (strokeWidth = _, ridge) : strokeWidth;
  };

  ridge.kernel = function (_) {
    return arguments.length ? (kernel = _, ridge) : kernel;
  };

  ridge.kernelFineness = function (_) {
    return arguments.length ? (kernelFineness = _, ridge) : kernelFineness;
  };

  ridge.axisDensityTicks = function (_) {
    return arguments.length ? (axisDensityTicks = _, ridge) : axisDensityTicks;
  };

  ridge.axisXTicks = function (_) {
    return arguments.length ? (axisXTicks = _, ridge) : axisXTicks;
  };

  ridge.mouseover = function (_) {
    return arguments.length ? (mouseover = _, ridge) : mouseover;
  };

  ridge.mouseleave = function (_) {
    return arguments.length ? (mouseleave = _, ridge) : mouseleave;
  };

  ridge.axisX = function (_) {
    return arguments.length ? (axisX = _, ridge) : axisX;
  };

  ridge.axisY = function (_) {
    return arguments.length ? (axisY = _, ridge) : axisY;
  };

  ridge.axisYDensity = function (_) {
    return arguments.length ? (axisYDensity = _, ridge) : axisYDensity;
  };

  ridge.gAxisX = function (_) {
    return arguments.length ? (gAxisX = _, ridge) : gAxisX;
  };

  ridge.gAxisY = function (_) {
    return arguments.length ? (gAxisY = _, ridge) : gAxisY;
  };

  ridge.gAxisYDensity = function (_) {
    return arguments.length ? (gAxisYDensity = _, ridge) : gAxisYDensity;
  };

  ridge.areaContainer = function (_) {
    return arguments.length ? (areaContainer = _, ridge) : areaContainer;
  };

  ridge.namespace = function (_) {
    return arguments.length ? (namespace = _, ridge) : namespace;
  };

  ridge.zoom = function (_) {
    return arguments.length ? (zoom$$1 = _, ridge) : zoom$$1;
  };

  ridge.zoomed = function (_) {
    return arguments.length ? (zoomed = _, ridge) : zoomed;
  };

  ridge.gridlinesX = function (_) {
    return arguments.length ? (gridlinesX = _, ridge) : gridlinesX;
  };

  ridge.gridlinesY = function (_) {
    return arguments.length ? (gridlinesY = _, ridge) : gridlinesY;
  };

  ridge.includeDensityScale = function (_) {
    return arguments.length ? (includeDensityScale = _, ridge) : includeDensityScale;
  };

  ridge.applyZoom = function (_) {
    return arguments.length ? (applyZoom = _, ridge) : applyZoom;
  };

  function ridge() {
    // extract data needed for calculating densities as well as axes ranges
    dataKeys = keys(data);
    var flatValues = [];
    dataValues = dataKeys.map(function (k, i) {
      var values = valueExtractor(k, i);
      flatValues = flatValues.concat(values);
      return values;
    });
    dataExtent = extent(flatValues);

    var _dataExtent = dataExtent,
        _dataExtent2 = _slicedToArray(_dataExtent, 2),
        dataMin = _dataExtent2[0],
        dataMax = _dataExtent2[1];

    if (savedZoomState) {
      scaleX //= savedZoomState.rescaleX(scaleX)
      .domain(savedZoomState.rescaleX(scaleX).domain()).range(savedZoomState.rescaleX(scaleX).range());
    } else {
      scaleX.range([0, spaceX]).domain([dataMin, dataMax]);
    } // ridges at most as tall as the space between them


    categorySpaceY = spaceY / (dataKeys.length + 1);
    adjustedSpaceY = spaceY - categorySpaceY; // set up clipping for ridges and y axis

    var defs = container.select('defs');
    if (defs.empty()) defs = container.append('defs').attr('class', 'definitions'); // clip for ridges

    var gClip = defs.select("clipPath#".concat(namespace, "-clip-path-areas"));
    if (gClip.empty()) gClip = defs.append('clipPath').attr('id', "".concat(namespace, "-clip-path-areas"));
    var cpRect = gClip.select('rect');
    if (cpRect.empty()) cpRect = gClip.append('rect');
    cpRect.attr('x', 0).attr('y', 0).attr('width', spaceX).attr('height', spaceY); // clip for primary y

    gClip = defs.select("clipPath#".concat(namespace, "-clip-path-y-axis"));
    if (gClip.empty()) gClip = defs.append('clipPath').attr('id', "".concat(namespace, "-clip-path-y-axis"));
    cpRect = gClip.select('rect');
    if (cpRect.empty()) cpRect = gClip.append('rect');
    cpRect.attr('x', -spaceX).attr('y', 0).attr('width', 2 * spaceX).attr('height', spaceY + 5);
    defs.raise(); // x axis

    axisX = axisBottom(scaleX).ticks(axisXTicks);
    if (gridlinesX) axisX.tickSize(-spaceY);
    gAxisX = container.select('g.x-axis');
    if (gAxisX.empty()) gAxisX = container.append('g').attr('class', 'x-axis');
    gAxisX.transition(transitionDuration).ease(easeFn).attr('transform', "translate(".concat(0, ", ", spaceY, ")")).call(axisX); // primary y axis

    scaleYCategories = scaleBand().domain(dataKeys).paddingInner(1);

    if (savedZoomState) {
      scaleYCategories.range([spaceY, spaceY - adjustedSpaceY].map(function (d) {
        return savedZoomState.applyY(d);
      }));
    } else {
      scaleYCategories.range([spaceY, spaceY - adjustedSpaceY]);
    }

    axisY = axisLeft(scaleYCategories);
    if (gridlinesY) axisY.tickSize(-spaceX);
    gAxisY = container.select('g.y-axis');
    if (gAxisY.empty()) gAxisY = container.append('g').attr('class', 'y-axis');
    gAxisY.transition(transitionDuration).ease(easeFn).call(axisY); // calculate densities

    var ticks = scaleX.ticks(kernelFineness);
    var kde = kernelDensityEstimator(kernelEpanechnikov(kernel), ticks);
    var densities = [];
    var maxDensitiy = 0;
    dataKeys.forEach(function (k, i) {
      var density = kde(dataValues[i]);
      density.forEach(function (_ref) {
        var _ref2 = _slicedToArray(_ref, 2),
            x = _ref2[0],
            y = _ref2[1];

        if (y > maxDensitiy) maxDensitiy = y;
      });
      densities.push(density);
    }); // density y-axis

    if (savedZoomState) {
      scaleY //= savedZoomState.rescaleY(scaleY)
      .domain(savedZoomState.rescaleY(scaleY).domain()).range(savedZoomState.rescaleY(scaleY).range());
    } else {
      scaleY.domain([0, maxDensitiy]).range([categorySpaceY, 0]);
    } // density scale at bottom right


    if (includeDensityScale) {
      axisYDensity = axisRight(scaleY).ticks(axisDensityTicks);
      gAxisYDensity = container.select('g.d-axis');
      if (gAxisYDensity.empty()) gAxisYDensity = container.append('g').attr('class', 'd-axis');
      gAxisYDensity.transition(transitionDuration).ease(easeFn).attr('transform', "translate(".concat(spaceX, ",").concat(spaceY - categorySpaceY, ")")).call(axisYDensity).select('.domain').remove();
    } // conatiner for clips


    var areaClipContainer = container.select('g.areas-clip');
    if (areaClipContainer.empty()) areaClipContainer = container.append('g').attr('class', 'areas-clip'); // container for all ridges (for applying transforms to)

    areaContainer = areaClipContainer.select('g.areas');
    if (areaContainer.empty()) areaContainer = areaClipContainer.append('g').attr('class', 'areas');
    var areas = areaContainer.selectAll('path.density-curve').data(dataKeys);
    areas.exit().remove();
    areas = areas.merge(areas.enter().append('path').attr('class', 'density-curve')); // apply clip paths

    gAxisY.attr('clip-path', "url(#".concat(namespace, "-clip-path-y-axis)"));
    areaClipContainer.attr('clip-path', "url(#".concat(namespace, "-clip-path-areas)")); // the line which return the svg path d attribute

    var densityCurve = line().curve(curve).x(function (d) {
      return scaleX(d[0]);
    }).y(function (d) {
      return scaleY(d[1]);
    }); // fix d3.line drawing auto closing rather than drawing down to the axis

    var ensureFlat = function ensureFlat(d, i) {
      var min = [dataMin, 0];
      var max = [dataMax, 0];
      return [min].concat(densities[i]).concat([max]);
    };

    areas.transition(transitionDuration).ease(easeFn).attr('transform', function (d, i) {
      var x = 0,
          y = scaleYCategories(d) - categorySpaceY;
      return "translate(".concat(x, ",").concat(y, ")");
    }).attr('d', function (d, i) {
      return densityCurve(ensureFlat(d, i));
    }).attr('fill', function (k, i) {
      return colorExtractor(k, i);
    }).attr("stroke", stroke).attr("stroke-width", strokeWidth).attr('opacity', opacity);
    areas.on('mouseenter', mouseover).on('mouseover', mouseover).on('mouseleave', mouseleave).on('mouseexit', mouseleave);

    if (applyZoom) {
      zoom$$1.on('zoom', zoomed);
      container.call(zoom$$1);
    } // if (savedZoomState) {
    //   areaContainer.attr("transform", savedZoomState);
    // }

  }

  return ridge;
}

var d3_ridge = {
  ridge: ridge,
  math: math,
  utils: utils
};

if (typeof window !== 'undefined') {
  window.d3_ridge = d3_ridge;
}

export default d3_ridge;
export { ridge, math, utils };
