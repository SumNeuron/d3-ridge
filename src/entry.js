import { event, mouse} from 'd3-selection';

import ridge from './modules/ridge'
import * as math from './modules/math'
import * as utils from './modules/utils'


const d3_ridge = {
  ridge, math, utils
}

if (typeof window !== 'undefined') {
  window.d3_ridge = d3_ridge;
}


export default d3_ridge
export {ridge, math, utils}
