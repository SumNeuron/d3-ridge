/**
* Extracts x and y of translate from transform property
* @param {string} transform transform property of svg element
* @returns {number[]} x, y of translate(x, y)
*/
export function getTranslation(transform) {
  // Create a dummy g for calculation purposes only. This will never
  // be appended to the DOM and will be discarded once this function
  // returns.
  var g = document.createElementNS('http://www.w3.org/2000/svg', 'g');
  // Set the transform attribute to the provided string value.
  transform = transform == undefined ? 'translate(0,0)' : transform;
  g.setAttributeNS(null, 'transform', transform);
  // consolidate the SVGTransformList containing all transformations
  // to a single SVGTransform of type SVG_TRANSFORM_MATRIX and get
  // its SVGMatrix.
  var matrix = g.transform.baseVal.consolidate().matrix;
  // As per definition values e and f are the ones for the translation.
  return [matrix.e, matrix.f];
}

export function resizeDebounce(f, wait) {
  var resize = debounce(function(){f()},wait)
  window.addEventListener('resize', resize)
}


export function debounce(func, wait, immediate) {
  var timeout;
    return function() {
        var context = this, args = arguments;
        var later = function() {
            timeout = null;
            if (!immediate) func.apply(context, args);
        };
        var callNow = immediate && !timeout;
        clearTimeout(timeout);
        timeout = setTimeout(later, wait);
        if (callNow) func.apply(context, args);
    };
}
