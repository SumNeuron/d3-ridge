import * as d3 from 'd3'
import {kernelDensityEstimator, kernelEpanechnikov} from './math'
import {getTranslation} from './utils'

export default function ridge(container) {
  let data,
  spaceX,
  spaceY,
  dataKeys,
  dataValues,
  dataExtent,
  scaleX = d3.scaleLinear(),
  scaleY = d3.scaleLinear(),
  valueExtractor = (key, index) => data[key],
  curve = d3.curveBasis,
  colorExtractor = (key, index) => {
    return d3.scaleSequential()
    .interpolator(d3.interpolateViridis)
    (index / dataKeys.length)
  },
  opacity = 0.7,
  stroke = (k, i)=>'black',
  strokeWidth = 0.1,
  kernel = 7,
  kernelFineness = 40,
  axisDensityTicks = 4,
  axisXTicks = 5,

  mouseover = (k, i) => {},
  mouseleave = (k, i) => {},

  namespace = 'ridge',

  axisX,
  axisY,
  axisYDensity,
  gAxisX,
  gAxisY,
  gAxisYDensity,
  areaContainer,
  categorySpaceY,
  adjustedSpaceY,
  scaleYCategories,
  gridlinesX = true,
  gridlinesY = true,
  includeDensityScale = true,
  applyZoom = true,
  transitionDuration = 500,
  easeFn = d3.easeSin,
  zoom = d3.zoom(),
  savedZoomState,
  zoomed = function () {
    savedZoomState = d3.event.transform
    areaContainer.attr("transform", savedZoomState);

    gAxisX
    // .transition(transitionDuration)
    // .ease(easeFn)
    .call(axisX.scale(savedZoomState.rescaleX(scaleX)));

    scaleYCategories.range([spaceY, spaceY-adjustedSpaceY].map(d=>savedZoomState.applyY(d)))
    gAxisY
    // .transition(transitionDuration)
    // .ease(easeFn)
    .call(axisY);

    scaleY.range([categorySpaceY ,0].map(d=>savedZoomState.applyY(d)))

    if (includeDensityScale) {
      gAxisYDensity
      // .transition(transitionDuration)
      // .ease(easeFn)
      .call(axisYDensity)
      .attr('transform', `translate(${spaceX},${spaceY-savedZoomState.applyY(categorySpaceY)})`)
      .select('.domain').remove()

      let _max = savedZoomState.applyY(spaceY-adjustedSpaceY)
      gAxisYDensity.selectAll('.tick').each(function(d, i){
        let dthis = d3.select(this)
        let [x, y] = getTranslation(dthis.attr('transform'))
        // can't figure out how to remove ticks above 0
      })
    }
  }

  ridge.data = function(_) { return arguments.length ? (data = _, ridge) : data; };
  ridge.spaceX = function(_) { return arguments.length ? (spaceX = _, ridge) : spaceX; };
  ridge.spaceY = function(_) { return arguments.length ? (spaceY = _, ridge) : spaceY; };
  ridge.dataKeys = function(_) { return arguments.length ? (dataKeys = _, ridge) : dataKeys; };
  ridge.dataValues = function(_) { return arguments.length ? (dataValues = _, ridge) : dataValues; };
  ridge.dataExtent = function(_) { return arguments.length ? (dataExtent = _, ridge) : dataExtent; };
  ridge.scaleX = function(_) { return arguments.length ? (scaleX = _, ridge) : scaleX; };
  ridge.scaleY = function(_) { return arguments.length ? (scaleY = _, ridge) : scaleY; };
  ridge.valueExtractor = function(_) { return arguments.length ? (valueExtractor = _, ridge) : valueExtractor; };
  ridge.curve = function(_) { return arguments.length ? (curve = _, ridge) : curve; };
  ridge.colorExtractor = function(_) { return arguments.length ? (colorExtractor = _, ridge) : colorExtractor; };
  ridge.opacity = function(_) { return arguments.length ? (opacity = _, ridge) : opacity; };
  ridge.stroke = function(_) { return arguments.length ? (stroke = _, ridge) : stroke; };
  ridge.strokeWidth = function(_) { return arguments.length ? (strokeWidth = _, ridge) : strokeWidth; };
  ridge.kernel = function(_) { return arguments.length ? (kernel = _, ridge) : kernel; };
  ridge.kernelFineness = function(_) { return arguments.length ? (kernelFineness = _, ridge) : kernelFineness; };
  ridge.axisDensityTicks = function(_) { return arguments.length ? (axisDensityTicks = _, ridge) : axisDensityTicks; };
  ridge.axisXTicks = function(_) { return arguments.length ? (axisXTicks = _, ridge) : axisXTicks; };
  ridge.mouseover = function(_) { return arguments.length ? (mouseover = _, ridge) : mouseover; };
  ridge.mouseleave = function(_) { return arguments.length ? (mouseleave = _, ridge) : mouseleave; };

  ridge.axisX = function(_) { return arguments.length ? (axisX = _, ridge) : axisX; };
  ridge.axisY = function(_) { return arguments.length ? (axisY = _, ridge) : axisY; };
  ridge.axisYDensity = function(_) { return arguments.length ? (axisYDensity = _, ridge) : axisYDensity; };
  ridge.gAxisX = function(_) { return arguments.length ? (gAxisX = _, ridge) : gAxisX; };
  ridge.gAxisY = function(_) { return arguments.length ? (gAxisY = _, ridge) : gAxisY; };
  ridge.gAxisYDensity = function(_) { return arguments.length ? (gAxisYDensity = _, ridge) : gAxisYDensity; };
  ridge.areaContainer = function(_) { return arguments.length ? (areaContainer = _, ridge) : areaContainer; };

  ridge.namespace = function(_) { return arguments.length ? (namespace = _, ridge) : namespace; };

  ridge.zoom = function(_) { return arguments.length ? (zoom = _, ridge) : zoom; };
  ridge.zoomed = function(_) { return arguments.length ? (zoomed = _, ridge) : zoomed; };

  ridge.gridlinesX = function(_) { return arguments.length ? (gridlinesX = _, ridge) : gridlinesX; };
  ridge.gridlinesY = function(_) { return arguments.length ? (gridlinesY = _, ridge) : gridlinesY; };
  ridge.includeDensityScale = function(_) { return arguments.length ? (includeDensityScale = _, ridge) : includeDensityScale; };
  ridge.applyZoom = function(_) { return arguments.length ? (applyZoom = _, ridge) : applyZoom; };





  function ridge() {
    // extract data needed for calculating densities as well as axes ranges

    dataKeys = d3.keys(data)
    let flatValues = []
    dataValues = dataKeys.map((k,i)=>{
      let values = valueExtractor(k, i)
      flatValues = flatValues.concat(values)
      return values
    })

    dataExtent = d3.extent(flatValues)
    let [dataMin, dataMax] = dataExtent

    if (savedZoomState) {
      scaleX //= savedZoomState.rescaleX(scaleX)
      .domain(savedZoomState.rescaleX(scaleX).domain())
      .range(savedZoomState.rescaleX(scaleX).range())
    }
    else {
      scaleX.range([0, spaceX]).domain([dataMin, dataMax])
    }
    // ridges at most as tall as the space between them
    categorySpaceY = spaceY / (dataKeys.length+1)
    adjustedSpaceY = spaceY - categorySpaceY


    // set up clipping for ridges and y axis
    let defs = container.select('defs')
    if (defs.empty()) defs = container.append('defs').attr('class', 'definitions')

    // clip for ridges
    let gClip = defs.select(`clipPath#${namespace}-clip-path-areas`)
    if (gClip.empty()) gClip = defs.append('clipPath').attr('id', `${namespace}-clip-path-areas`)

    let cpRect = gClip.select('rect')
    if (cpRect.empty()) cpRect = gClip.append('rect')
    cpRect
    .attr('x', 0)
    .attr('y', 0)
    .attr('width', spaceX)
    .attr('height', spaceY)

    // clip for primary y
    gClip = defs.select(`clipPath#${namespace}-clip-path-y-axis`)
    if (gClip.empty()) gClip = defs.append('clipPath').attr('id', `${namespace}-clip-path-y-axis`)
    cpRect = gClip.select('rect')
    if (cpRect.empty()) cpRect = gClip.append('rect')
    cpRect
    .attr('x', -spaceX)
    .attr('y', 0)
    .attr('width', 2*spaceX)
    .attr('height', spaceY+5)

    defs.raise()




    // x axis
    axisX = d3.axisBottom(scaleX).ticks(axisXTicks)
    if (gridlinesX) axisX.tickSize(-spaceY)
    gAxisX = container.select('g.x-axis')
    if (gAxisX.empty()) gAxisX = container.append('g').attr('class', 'x-axis')
    gAxisX
    .transition(transitionDuration)
    .ease(easeFn)
    .attr('transform',`translate(${0}, ${spaceY})`)
    .call(axisX)



    // primary y axis
    scaleYCategories = d3.scaleBand().domain(dataKeys).paddingInner(1)

    if (savedZoomState) {
      scaleYCategories.range([spaceY, spaceY-adjustedSpaceY].map(d=>savedZoomState.applyY(d)))
    } else {
      scaleYCategories.range([spaceY, spaceY-adjustedSpaceY])
    }

    axisY = d3.axisLeft(scaleYCategories)
    if (gridlinesY) axisY.tickSize(-spaceX)
    gAxisY = container.select('g.y-axis')
    if (gAxisY.empty()) gAxisY = container.append('g').attr('class', 'y-axis')
    gAxisY
    .transition(transitionDuration)
    .ease(easeFn)
    .call(axisY)


    // calculate densities
    let ticks = scaleX.ticks(kernelFineness)
    let kde = kernelDensityEstimator(kernelEpanechnikov(kernel), ticks)
    let densities = []
    let maxDensitiy = 0
    dataKeys.forEach((k, i) => {
      let density = kde(dataValues[i])
      density.forEach(([x, y])=>{
        if (y > maxDensitiy) maxDensitiy = y
      })
      densities.push(density)
    })


    // density y-axis
    if (savedZoomState) {
      scaleY //= savedZoomState.rescaleY(scaleY)
      .domain(savedZoomState.rescaleY(scaleY).domain())
      .range(savedZoomState.rescaleY(scaleY).range())
    } else {
      scaleY.domain([0, maxDensitiy]).range([categorySpaceY ,0])
    }

    // density scale at bottom right
    if (includeDensityScale) {
      axisYDensity = d3.axisRight(scaleY).ticks(axisDensityTicks)
      gAxisYDensity = container.select('g.d-axis')
      if (gAxisYDensity.empty()) gAxisYDensity = container.append('g').attr('class', 'd-axis')
      gAxisYDensity
      .transition(transitionDuration)
      .ease(easeFn)
      .attr('transform', `translate(${spaceX},${spaceY-categorySpaceY})`)
      .call(axisYDensity)
      .select('.domain').remove()
    }



    // conatiner for clips
    let areaClipContainer = container.select('g.areas-clip')
    if (areaClipContainer.empty()) areaClipContainer = container.append('g').attr('class', 'areas-clip')

    // container for all ridges (for applying transforms to)
    areaContainer = areaClipContainer.select('g.areas')
    if (areaContainer.empty()) areaContainer = areaClipContainer.append('g').attr('class', 'areas')


    let areas = areaContainer.selectAll('path.density-curve').data(dataKeys)
    areas.exit().remove()
    areas = areas.merge(areas.enter().append('path').attr('class', 'density-curve'))


    // apply clip paths
    gAxisY.attr('clip-path', `url(#${namespace}-clip-path-y-axis)`)
    areaClipContainer.attr('clip-path', `url(#${namespace}-clip-path-areas)`)


    // the line which return the svg path d attribute
    let densityCurve = d3.line().curve(curve)
    .x(d=>scaleX(d[0])).y(d=>scaleY(d[1]))

    // fix d3.line drawing auto closing rather than drawing down to the axis
    let ensureFlat = (d, i)=>{
      let min = [dataMin, 0]
      let max = [dataMax, 0]
      return [min].concat(densities[i]).concat([max])
    }

    areas
    .transition(transitionDuration)
    .ease(easeFn)
    .attr('transform', function(d, i){
      let x=0, y=scaleYCategories(d)-categorySpaceY
      return `translate(${x},${y})`
    })
    .attr('d', (d, i)=>densityCurve(ensureFlat(d, i)))
    .attr('fill', (k, i)=>colorExtractor(k, i))
    .attr("stroke", stroke)
    .attr("stroke-width", strokeWidth)
    .attr('opacity', opacity)

    areas
    .on('mouseenter',mouseover)
    .on('mouseover',mouseover)
    .on('mouseleave',mouseleave)
    .on('mouseexit',mouseleave)

    if (applyZoom) {
      zoom.on('zoom', zoomed)
      container.call(zoom)
    }
    // if (savedZoomState) {
    //   areaContainer.attr("transform", savedZoomState);
    // }


  }


  return ridge
}
